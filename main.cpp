#include "mainwindow.h"

#include <QApplication>
#include <QLocale>
#include <QTranslator>
#include <QLibraryInfo>
#include <QSystemTrayIcon>
#include <QMessageBox>
#include <QDebug>
int main(int argc, char *argv[])
{
  QApplication a(argc, argv);
  //载入翻译文件
  QTranslator translator;
  if (translator.load("qt_zh_CN", QLibraryInfo::path(QLibraryInfo::TranslationsPath)))
  {
    a.installTranslator(&translator);
  }
  //实例化窗口
  MainWindow w;
  //设置窗口的图标
  QIcon icon(":/testResource/icon.png");
  w.setWindowIcon(icon);
  //展示窗口
  w.show();
  //关闭窗口时，不关闭系统托盘
  QApplication::setQuitOnLastWindowClosed(false);
  return a.exec();
}
