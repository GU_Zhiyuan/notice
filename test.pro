QT       += core gui
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++17

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    contentsetting.cpp \
    icon.cpp \
    main.cpp \
    mainwindow.cpp \
    timesetting.cpp \
    versiondialog.cpp

HEADERS += \
    contentsetting.h \
    icon.h \
    mainwindow.h \
    timesetting.h \
    versiondialog.h

FORMS += \
    contentsetting.ui \
    mainwindow.ui \
    timesetting.ui \
    versiondialog.ui

TRANSLATIONS += \
    test_zh_CN.ts
CONFIG += lrelease
CONFIG += embed_translations

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

DISTFILES += \
    icon.png \
    icons/icon.png

RESOURCES += \
    resource.qrc
