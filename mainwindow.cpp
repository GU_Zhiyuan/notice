#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QMessageBox>
#include <QCloseEvent>
#include <QDateTime>
#include <QSystemTrayIcon>
#include "versiondialog.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    lastTimeLabel = ui->lastTime; //初始化倒计时label
    correctTimeLabel = ui->correctTime; //初始化当前时间label
    currectTimer = new QTimer(this); //初始化当前时间的timer
    lastTimer = new QTimer(this); //初始化倒计时timer
    settings = new QSettings("resat", "notice"); // 初始化 QSettings 对象
    trayIcon = new icon(this); // 初始化 icon 对象
    connect(trayIcon, &icon::leftClicked, this,&MainWindow::createIconShow);
    connect(ui->actionVersion,SIGNAL(triggered(bool)),this,SLOT(showVersionWindow()));
    createTimeLabel();
    checkOnOrClose();
}

MainWindow::~MainWindow()
{
    delete ui;
}
void MainWindow::closeEvent(QCloseEvent *event)
{
    QMessageBox::StandardButton resBtn = QMessageBox::question(this, "关闭确认",
                                                               "确定关闭窗口吗？",
                                                               QMessageBox:: No | QMessageBox:: Yes,
                                                               QMessageBox::Yes);
    if (resBtn == QMessageBox::Yes) {
        event->accept();
    } else {
        event->ignore();
    }
}

void MainWindow::createIconShow(QSystemTrayIcon::ActivationReason reason) {
    if (reason == QSystemTrayIcon::Trigger) {
        this->showNormal();
    }
}

void MainWindow::createTimeLabel() {
    // 设置定时器每秒更新时间
    connect(currectTimer, &QTimer::timeout,this, [this]() {
        QDateTime currentTime = QDateTime::currentDateTime();
        this->correctTimeLabel->setText(currentTime.toString("yyyy-MM-dd HH:mm:ss"));
    });
    currectTimer->start(1000); // 每1000毫秒（1秒）触发一次
}

void MainWindow::checkOnOrClose() {
    bool  isOnOrClose = settings->value("onOrClose", false).toBool();
    QRadioButton *radioon  =  ui->radioButton_on;
    QRadioButton *radioclose  =  ui->radioButton_close;
    // 连接第一个单选按钮的clicked信号到自定义的槽函数
    connect(radioon, &QRadioButton::clicked, this, &MainWindow::onRadioOnClicked);
    // 连接第二个单选按钮的clicked信号到自定义的槽函数
    connect(radioclose, &QRadioButton::clicked, this, &MainWindow::onRadioCloseClicked);
    if(isOnOrClose){
        radioon->setChecked(true);
        radioclose->setChecked(false);
        getOldTime();
    }else{
        radioon->setChecked(false);
        radioclose->setChecked(true);
        getOldTime();
        lastTimeLabel->setText("停止计时");
    }
}
void MainWindow::onRadioOnClicked(bool checked)
{
    if (checked) {
        settings->setValue("onOrClose", true);
        trayIcon->showToast("开启通知",3000);
        getOldTime();
    }
}

void MainWindow::onRadioCloseClicked(bool checked)
{
    if (checked) {
        settings->setValue("onOrClose", false);
        trayIcon->showToast("关闭通知",3000);
        lastTimer->stop();
        lastTimeLabel->setText("停止计时");
    }
}

int MainWindow::on_submitButton_clicked()
{
    QString text = ui->noticeTime->toPlainText();
    if(text  == ""){
        QMessageBox::critical(this, "错误", "间隔时间不得为空");
        return -1;
    }
    bool ok;
    int value = text.toInt(&ok);//小于2147483647
    // 检查转换是否成功，以及转换后的值是否为正数
    if (ok && value > 0) {
        settings->setValue("time", value);
        // 如果是正整数
    } else {
        QMessageBox::information(this, "提示", "请确保输入的是正整数！");
        return -1;
    }
}

void MainWindow::getOldTime()
{
    QString value = settings->value("time", "").toString();
    bool  isOnOrClose = settings->value("onOrClose", false).toBool();
    if(value!=""){
        ui->noticeTime->setText(value);
        if(isOnOrClose){
            int valueInt = value.toInt();
            startCountdown(valueInt);
        }
    }
}

void MainWindow::startCountdown(int minute)
{
    // 创建一个 QTime 对象，初始时间为 00:00:00
    QTime *timeRemaining = new QTime(0, 0, 0);
    // 通过添加秒数来设置时间为90分钟后
    // 90分钟 * 60秒/分钟 = 5400秒
    *timeRemaining =  timeRemaining->addSecs(minute * 60);
    QString timeStr = timeRemaining->toString("HH:mm:ss");
    qDebug() << timeStr;
    // 设置定时器每秒触发一次
    connect(lastTimer, &QTimer::timeout, this, [=]() {
        *timeRemaining = timeRemaining->addSecs(-1);
        lastTimeLabel->setText(timeRemaining->toString("HH:mm:ss"));
        // 如果时间到了，停止计时器
        if (timeRemaining->toString("HH:mm:ss") == "00:00:00") {
            lastTimer->stop();
            triggerNotice();
        }    });
    lastTimer->start(1000); // 1000毫秒触发一次
    // 初始化标签显示剩余时间
    lastTimeLabel->setText(timeRemaining->toString("HH:mm:ss"));
}
void MainWindow::triggerNotice(){
    trayIcon->showToast("触发通知",5000);
    getOldTime();
}

void MainWindow::showVersionWindow(){
    VersionDialog * dialog = new VersionDialog;
    dialog->show();
}
