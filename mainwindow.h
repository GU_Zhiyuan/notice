#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "icon.h"
#include <QSettings>
#include <QLabel>
#include <QTimer>
QT_BEGIN_NAMESPACE
namespace Ui {
class MainWindow;
}
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

protected:
    void closeEvent(QCloseEvent *event) override;  // 声明重写的函数

private:
    void createTimeLabel();
    void checkOnOrClose();
    void getOldTime();
    void startCountdown(int minute);
    void triggerNotice();
private:
    Ui::MainWindow *ui;
    icon *trayIcon; // 声明一个SystemTray对象的指针
    QSettings *settings; // 声明 QSettings 为成员变量
    QLabel *correctTimeLabel;
    QLabel *lastTimeLabel;
    QTimer *currectTimer;
    QTimer *lastTimer;
private slots:
    void onRadioOnClicked(bool checked);
    void onRadioCloseClicked(bool checked);
    int on_submitButton_clicked();
    void createIconShow(QSystemTrayIcon::ActivationReason reason);
    void showVersionWindow();
};
#endif // MAINWINDOW_H
