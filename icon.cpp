#include "icon.h"
#include <QIcon> // 包含QIcon类，用于设置图标
#include <QApplication>
icon::icon(QObject *parent) : QObject(parent)
{
    // 创建系统托盘图标
    trayIcon = new QSystemTrayIcon(this);
    // 设置系统托盘图标的图标，这里假设有一个icon.png作为图标
    trayIcon->setIcon(QIcon(":/testResource/icon.png"));
    // 创建右键菜单
    QMenu *menu = new QMenu;
    // 添加一个退出动作
    QAction *quitAction = menu->addAction("退出");
    connect(quitAction, &QAction::triggered, qApp, &QCoreApplication::quit);
    connect(trayIcon, &QSystemTrayIcon::activated, this, &icon::onActivated);
    // 将菜单添加到系统托盘图标
    trayIcon->setContextMenu(menu);
    // 显示系统托盘图标
    trayIcon->show();
}

void icon::showToast(const std::string &toast,const int time) {
    QString qToast = QString::fromStdString(toast);
    trayIcon->showMessage("通知", qToast, QSystemTrayIcon::Information, time);
}

void icon:: onActivated(QSystemTrayIcon::ActivationReason reason)
{
    if (reason == QSystemTrayIcon::Trigger) {
        // 发射左键单击信号
        emit leftClicked(reason);
    }
}
