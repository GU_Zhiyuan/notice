#ifndef ICON_H
#define ICON_H
#include <QSystemTrayIcon>
#include <QObject>
#include <QMenu> // 包含QMenu类，用于创建右键菜单
class icon : public QObject
{
    Q_OBJECT
public:
    icon(QObject *parent = nullptr);
    void showToast( const std::string &toast,const int time);
    void triggerActivatedEvent();
signals:
    void leftClicked(QSystemTrayIcon::ActivationReason reason); // Modified to include the activation reason

private:
    QSystemTrayIcon *trayIcon; // 系统托盘图标对象

private slots:
    void onActivated(QSystemTrayIcon::ActivationReason reason);
};
#endif // ICON_H
