#ifndef CONTENTSETTING_H
#define CONTENTSETTING_H

#include <QDialog>

namespace Ui {
class contentSetting;
}

class contentSetting : public QDialog
{
    Q_OBJECT

public:
    explicit contentSetting(QWidget *parent = nullptr);
    ~contentSetting();

private:
    Ui::contentSetting *ui;
};

#endif // CONTENTSETTING_H
