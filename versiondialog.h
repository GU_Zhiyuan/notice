#ifndef VERSIONDIALOG_H
#define VERSIONDIALOG_H

#include <QDialog>
#include <QLabel>
namespace Ui {
class VersionDialog;
}

class VersionDialog : public QDialog
{
    Q_OBJECT

public:
    explicit VersionDialog(QWidget *parent = nullptr);
    ~VersionDialog();

private slots:
    void on_pushButton_clicked();
    void on_projectlabel_linkActivated(const QString &link);

private:
    Ui::VersionDialog *ui;
    QLabel *linkLabel;
};

#endif // VERSIONDIALOG_H
