#ifndef TIMESETTING_H
#define TIMESETTING_H

#include <QDialog>

namespace Ui {
class timeSetting;
}

class timeSetting : public QDialog
{
    Q_OBJECT

public:
    explicit timeSetting(QWidget *parent = nullptr);
    ~timeSetting();

private:
    Ui::timeSetting *ui;
};

#endif // TIMESETTING_H
