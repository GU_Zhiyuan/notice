#include "versiondialog.h"
#include "ui_versiondialog.h"
#include "QDesktopServices"
VersionDialog::VersionDialog(QWidget *parent)
    : QDialog(parent)
    , ui(new Ui::VersionDialog)
{
    ui->setupUi(this);
    linkLabel = ui->projectlabel; //初始化倒计时label
    linkLabel->setText("<a style='color: black; text-decoration: none' href = https://gitee.com/GU_Zhiyuan/notice>项目地址：https://gitee.com/GU_Zhiyuan/notice");
}

VersionDialog::~VersionDialog()
{
    delete ui;
}

void VersionDialog::on_pushButton_clicked()
{
    this->close();
}

void VersionDialog::on_projectlabel_linkActivated(const QString &link)
{
    qDebug() << "44";
    QDesktopServices::openUrl(QUrl(link));
}

